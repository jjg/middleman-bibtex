lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'middleman-bibtex/version'

Gem::Specification.new do |spec|
  spec.name          = 'middleman-bibtex'
  spec.version       = Middleman::Bibtex::VERSION
  spec.authors       = ['J. J. Green']
  spec.email         = ['j.j.green@gmx.co.uk']
  spec.description   = <<-EOF
                       A middleman extension for BibTeX bibliographies,
                       a fork of the middleman-citation extension.
                       EOF
  spec.summary       = %q{Middleman extension for BibTeX bibliographies}
  spec.homepage      = 'https://gitlab.com/jjg/middleman-bibtex'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'bibtex-ruby'
  spec.add_runtime_dependency 'citeproc-ruby', ['< 1.0']
  spec.add_runtime_dependency 'middleman-core', ['~> 4.0']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'capybara'
  spec.add_development_dependency 'middleman', ['~> 4.0']
end
