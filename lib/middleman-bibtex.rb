require 'middleman-bibtex/version'
require 'middleman-core'
require 'bibtex'
require 'citeproc'

module BibtexHelper
  extend self

  def citations_search(bib, search_key, author = nil)
    entries_matching_key = bib.query(search_key)
    entries =
      if author then
        search_by_author(entries_matching_key, author)
      else
        entries_matching_key
      end
    entries
      .sort_by { |x| x.year.to_i }
      .map(&:key)
  end

  def citation_formatted(entry, style, format)
    CiteProc.process(entry, :style => style, :format => format)
  end

  def citation_entry(bib, key)
    unless entry = bib[key] then
      message = format('No entry for key %s', key)
      raise Middleman::Bibtex::KeyNotFound, message
    end
    entry.convert_latex.to_citeproc
  end

  def search_by_author(entries, author)
    bib_author = BibTeX::Name.parse(author)
    entries.select do |e|
      e.respond_to?(:author) &&
        e.author &&
        e.author.include?(bib_author)
    end
  end

end

module Middleman

  class BibtexExtension < Middleman::Extension

    option :path, nil, 'Path to the bibtex file'
    option :style, 'chicago-author-date', 'BibTeX style'
    option :format, 'html', 'Bibliography format'

    def initialize(app, options_hash = {}, &block)
      super
      app.config[:bib] = BibTeX.open(options.path)
      app.config[:cite_style] = options.style
      app.config[:cite_format] = options.format
    end

    helpers do

      def citations_search(search_key, author = nil)
        bib = app.config[:bib]
        BibtexHelper.citations_search(bib, search_key, author)
      end

      def citation_entry(key)
        bib = app.config[:bib]
        BibtexHelper.citation_entry(bib, key)
      end

      def citation_formatted(entry)
        style = app.config[:cite_style]
        format = app.config[:cite_format]
        BibtexHelper.citation_formatted(entry, style, format)
      end

      def citation(key)
        citation_formatted(citation_entry(key))
      end
    end
  end
end

Middleman::Extensions.register(:bibtex, Middleman::BibtexExtension)
