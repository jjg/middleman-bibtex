module Middleman
  module Bibtex
    VERSION = '0.3.1'
    class KeyNotFound < RuntimeError ; end
  end
end
