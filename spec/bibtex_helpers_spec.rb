require 'bibtex'
require 'middleman-bibtex'
require_relative 'spec_helper'

describe BibtexHelper, 'helpers' do
  let(:bib){ BibTeX.open(fixture('test.bib'), :filter => :latex) }

  describe 'citation_entry' do

    context 'for a key not in the bibliography' do

      it 'should raise' do
        expect {
          BibtexHelper.citation_entry(bib, 'no-such')
        }.to raise_error(Middleman::Bibtex::KeyNotFound)
      end
    end

    context 'for a key in the bibliography' do

      it 'should not raise' do
        expect {
          BibtexHelper.citation_entry(bib, 'godel:1933')
        }.to_not raise_error
      end

      describe 'the return value' do
        let(:result) { BibtexHelper.citation_entry(bib, 'godel:1933') }

        it 'should be a Hash' do
          expect(result).to be_a Hash
        end

        it 'should have the expected keys' do
          expect(result.keys)
            .to match_array ['author',
                             'container-title',
                             'id',
                             'issued',
                             'page',
                             'title',
                             'type',
                             'volume']
        end

        it 'should correctly split the author name' do
          expect(result['author'])
            .to match_array [ {'family' => 'Gödel', 'given' => 'K.'} ]
        end
      end
    end
  end

  describe 'citation_formatted' do
    let(:entry) { BibtexHelper.citation_entry(bib, 'godel:1933') }
    let(:result) { BibtexHelper.citation_formatted(entry, 'ieee', 'html') }

    it 'should return a String' do
      expect(result).to be_a String
    end

    it 'should have the expected author' do
      expect(result).to match /K. Gödel/
    end
  end

  describe 'citation_search' do

    shared_examples 'the expected search results' do

      it 'should be an Array' do
        expect(subject).to be_an Array
      end

      it 'should have the expected number of elements' do
        expect(subject.length).to eq results.length
      end

      it 'should have String elements' do
        subject.each { |item| expect(item).to be_a String }
      end

      it 'should have the expected elements' do
        expect(subject).to match_array results
      end

      it 'should be in year order' do
        expect(subject).to eq results
      end
    end

    context 'when search_term and author are nil' do
      it_should_behave_like 'the expected search results' do
        let(:subject) {
          BibtexHelper.citations_search(bib, nil, nil)
        }
        let(:results) { ['godel:1933', 'turing:1936', 'turing:1946'] }
      end
    end

    context 'when search_term is "@article", but author is nil' do
      it_should_behave_like 'the expected search results' do
        let(:subject) {
          BibtexHelper.citations_search(bib, '@article', nil)
        }
        let(:results) { ['godel:1933', 'turing:1936'] }
      end
    end

    context 'when search_term is nil, but author is given' do
      it_should_behave_like 'the expected search results' do
        let(:subject) {
          BibtexHelper.citations_search(bib, nil, 'A. M. Turing')
        }
        let(:results) { ['turing:1936', 'turing:1946'] }
      end
    end

    context 'when search_term is "@article" and author is given' do
      it_should_behave_like 'the expected search results' do
        let(:subject) {
          BibtexHelper.citations_search(bib, '@article', 'A. M. Turing')
        }
        let(:results) { ['turing:1936'] }
      end
    end
  end
end
