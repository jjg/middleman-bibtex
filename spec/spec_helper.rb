require 'rspec'
require 'capybara/rspec'
require 'middleman-core'
require 'middleman-core/rack'

def fixture(subpath)
  absolute_path('fixtures', subpath)
end

def example_site
  fixture('site')
end

def absolute_path(*parts)
  File.expand_path(File.join(File.dirname(__FILE__), *parts))
end

ENV['MM_ROOT'] = example_site
middleman = Middleman::Application.new
Capybara.app = Middleman::Rack.new(middleman).to_app do
  set :environment, :development
  set :show_exceptions, false
end
