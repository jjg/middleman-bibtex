require 'spec_helper'

describe 'An example bibliography', :type => :feature do
  before do
    visit '/'
  end

  it 'has the correct title header' do
    expect(page).to have_selector 'h1'
    within 'h1' do
      expect(page).to have_content /Flexible bibliographies/i
    end
  end

  it 'has an unnumbered list' do
    expect(page).to have_selector 'ul'
  end

  context 'the list' do
    let(:items) { page.all('li') }

    it 'has the expected number of items' do
      expect(items.count).to eq 3
    end

    it 'has the expected ids' do
      expect(items.map { |item| item[:id] })
        .to match_array ['godel-1933', 'turing-1936', 'turing-1946']
    end
  end

  describe 'an item with a customised DOI link' do
    let(:item) { page.find('li#turing-1936') }

    it 'should be found' do
      expect(item).to_not be_nil
    end

    it 'should contain the expected DOI link' do
      expect(item).to have_link('DOI')
    end
  end
end
