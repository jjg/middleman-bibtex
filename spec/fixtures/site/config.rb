require 'middleman-bibtex'

def absolute_path(*parts)
  File.expand_path(File.join(File.dirname(__FILE__), *parts))
end

def fixture(file)
  absolute_path('..', file)
end

activate(:bibtex,
         :path => fixture('test.bib'),
         :style => 'ieee',
         :format => 'html')

helpers do

  # CSS freindly ids

  def id_tag(key)
    %Q|id="#{key.gsub(/:/,'-')}"|
  end

  # custom formatting of BibTEX DOI entries

  def citation_with_links(key)
    entry = citation_entry(key)
    links = []
    if doi = entry['DOI'] then
      links << link_to('DOI', "http://doi.org/#{doi}")
    end
    entry_html = citation_formatted(entry)
    if links.empty? then
      entry_html
    else
      links_html = '(' + links.join(', ') + ')'
      [entry_html, links_html].join(' ')
    end
  end

end
