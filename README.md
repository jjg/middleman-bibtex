# Middleman-bibtex

> This gem is broken for recent versions of Ruby, since barely
> used I probably won't fix that and will remove it from RubyGems
> at some point ...

An extension for the [Middleman](http://middlemanapp.com/) static
site generator for formating BibTeX bibliographies.  This is a fork of
[middleman-citation](https://github.com/gabysbrain/middleman-citation)
which offers similar functionailty.

An example of a Middleman site using this plugin can be found at
<https://jjg.gitlab.io/en/mathematics/papers/>.

## Installation

If using version 4 or higher Middleman then add

	gem 'middleman-bibtex'

to your Gemfile; for Middleman versions 3.* then instead use

	gem 'middleman-bibtex', '< 0.3'

then execute

    bundle install

Or install it yourself as:

    gem install middleman-bibtex

## Configuration

In your `config.rb` file for your site add:

```ruby
require 'middleman-bibtex'

activate :bibtex do |opts|
  opts.path = '/path/to/your.bib' # path to a bibtex file
  opts.style = 'ieee'             # style from citeproc-styles
  opts.format = 'html'            # output format
end
```

## Usage

This adds the following helper methods you can use in your Middleman
templates.

* `citations_search(search_term, author)`: Search the BibTeX file for all
   citations matching a search term (such as `'@article'`) and by the given
   author.  The `author` argument can be ommitted to match all authors and
   a `search_term` of `nil` will match all items in the bibliography.

* `citation(key)`: Given a BibTeX citation key as returned from
  `citations_search`, return a formatted string the citation according to
  how the `style` and `format` options were set.

Typical template:

```html+erb
<ul>
  <% citations_search(nil).each do |key|  %>
  <li id="<%= key %>">
    <%= citation(key) %>
  </li>
  <% end %>
</ul>
```

For extra control on the output, one can use:

* `citation_entry(key)`: Return the unformatted entry (a hash)
  corresponding to the BibTeX citation key.

* `citation_formatted(entry)`: Format an unformatted entry.

In fact the `citation` method is implemented using these:

```ruby
def citation(key)
  citation_formatted(citation_entry(key))
end
```

The point is that one can interrogate the unformatted entry to
add extra formatting: The following code adds a DOI link if the
entry matching the `key` has a `URL` field containing the DOI.

```ruby
entry = citation_entry(key)
entry_html = citation_formatted(entry)
unless (doi_url = entry['URL']).nil? then
  doi_link = format('(%s)', link_to('doi', doi_url))
end
[entry_html, doi_link].compact.join(' ')
```
